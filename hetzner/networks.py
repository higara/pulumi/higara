import pulumi
import pulumi_hcloud as hcloud

hetzner_network = hcloud.Network(
    "hetzner_network",
    ip_range="172.16.48.0/20",
    name="hnetwork",
    opts=pulumi.ResourceOptions(),
)

hetzner_network_subnet = hcloud.NetworkSubnet(
    "hetzner_network_subnet",
    ip_range="172.16.48.0/24",
    network_id=2687974,
    network_zone="eu-central",
    type="cloud",
    opts=pulumi.ResourceOptions(),
)

hetzner_leny = hcloud.NetworkRoute(
    "hetzner_leny",
    destination="172.16.0.0/20",
    gateway="172.16.48.5",
    network_id=2687974,
    opts=pulumi.ResourceOptions(),
)
