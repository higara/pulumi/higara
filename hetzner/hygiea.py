"""
hygiea - Firefly-iii server

Hygiea (minor planet designation: 10 Hygiea) is the fourth
largest asteroid in the Asteroid Belt and somewhat oblong.
It hosts Hygeia Station. Like many places in the Belt, its
population suffered from high UN (UN) taxes that made
survival expensive and kept the population routinely destitute.
Consequently, a robust black market was active, particularly
in usable hydroponics.

"""

import pulumi
import pulumi_digitalocean as digitalocean
import pulumi_hcloud as hcloud

from .networks import hetzner_network
from .sshkeys import stooj_yubikey5_hcloud

LOCATION = "fsn1"
IMAGE = "ubuntu-22.04"
SERVER_TYPE = "cx22"

hygiea_ip = hcloud.PrimaryIp(
    "hygiea-primary_ip",
    datacenter="fsn1-dc14",
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
    opts=pulumi.ResourceOptions(),
)

hygiea = hcloud.Server(
    "hygiea",
    backups=True,
    image=IMAGE,
    location=LOCATION,
    name="hygiea",
    server_type=SERVER_TYPE,
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=hygiea_ip.id.apply(lambda id: int(id)),
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id.apply(lambda id: int(id)),
            ip="172.16.48.11",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id],
    opts=pulumi.ResourceOptions(),
)

ginstoo_hygiea_a = digitalocean.DnsRecord(
    "ginstoo_hygiea_A",
    domain="ginstoo.net",
    name="hygiea",
    ttl=3600,
    type="A",
    value=hygiea_ip.ip_address,
    opts=pulumi.ResourceOptions(),
)

ginstoo_accounts_cname = digitalocean.DnsRecord(
    "ginstoo_accounts_CNAME",
    domain="ginstoo.net",
    name="accounts",
    ttl=3600,
    type="CNAME",
    value="hygiea.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)

ginstoo_accounts_importer_cname = digitalocean.DnsRecord(
    "ginstoo_accounts_in_CNAME",
    domain="ginstoo.net",
    name="accounts-in",
    ttl=3600,
    type="CNAME",
    value="hygiea.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)
