"""
kuiper - Audiobookshelf server

The Kuiper Belt sometimes called the Edgeworth–Kuiper belt, is
a circumstellar disc in the Sol system beyond the planets,
extending from the orbit of Neptune (at 30 AU) to approximately
50 AU from Sol. It is similar to the asteroid belt, but it is
far larger - 20 times as wide and 20 to 200 times as massive.
Like the asteroid belt, it consists mainly of small bodies, or
remnants from the Sol system's formation.

"""

import pulumi
import pulumi_digitalocean as digitalocean
import pulumi_hcloud as hcloud

from .networks import hetzner_network
from .sshkeys import stooj_yubikey5_hcloud

LOCATION = "fsn1"
IMAGE = "ubuntu-22.04"
SERVER_TYPE = "cx22"

kuiper_ip = hcloud.PrimaryIp(
    "kuiper-primary_ip",
    datacenter="fsn1-dc14",
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
    opts=pulumi.ResourceOptions(),
)

kuiper = hcloud.Server(
    "kuiper",
    backups=True,
    image=IMAGE,
    location=LOCATION,
    name="kuiper",
    server_type=SERVER_TYPE,
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=kuiper_ip.id.apply(lambda id: int(id)),
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id.apply(lambda id: int(id)),
            ip="172.16.48.10",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id],
    opts=pulumi.ResourceOptions(),
)

ginstoo_kuiper_a = digitalocean.DnsRecord(
    "ginstoo_kuiper_A",
    domain="ginstoo.net",
    name="kuiper",
    ttl=3600,
    type="A",
    value=kuiper_ip.ip_address,
    opts=pulumi.ResourceOptions(),
)

ginstoo_audiobooks_cname = digitalocean.DnsRecord(
    "ginstoo_audiobooks_CNAME",
    domain="ginstoo.net",
    name="audiobooks",
    ttl=3600,
    type="CNAME",
    value="kuiper.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)
