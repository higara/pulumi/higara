import pulumi
import pulumi_hcloud as hcloud

config = pulumi.Config()
stooj_yubikey5_value = config.require("stooj-yubikey5")

stooj_yubikey5_hcloud = hcloud.SshKey(
    "stooj-yubikey5-hcloud",
    name="stooj@yubikey5",
    public_key=stooj_yubikey5_value,
    opts=pulumi.ResourceOptions()
)
