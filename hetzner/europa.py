"""europa - a Jellyfin server for Ginstoo.

Europa is one of the brightest mooons in the Sol system, and Anna Volovodov
lived there before journeying to the ring.
"""

import pulumi
import pulumi_digitalocean as digitalocean
import pulumi_hcloud as hcloud

from .networks import hetzner_network
from .sshkeys import stooj_yubikey5_hcloud

europa_ip = hcloud.PrimaryIp(
    "europa-primary_ip",
    datacenter="fsn1-dc14",
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
    opts=pulumi.ResourceOptions(),
)

europa = hcloud.Server(
    "europa",
    backups=True,
    image="ubuntu-22.04",
    location="fsn1",
    name="europa",
    server_type="cpx31",
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=europa_ip.id.apply(lambda id: int(id)),
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id.apply(lambda id: int(id)),
            ip="172.16.48.9",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id],
    opts=pulumi.ResourceOptions(),
)

europa_jellyfin_cache_drive = hcloud.Volume(
    "europa-jellyfin_cache_drive",
    size=50,
    location="fsn1",
    format="ext4",
)

europa_jellyfin_cache_drive_attachment = hcloud.VolumeAttachment(
    "europa-jellyfin_cache_drive-attachment",
    volume_id=europa_jellyfin_cache_drive.id.apply(lambda id: int(id)),
    server_id=europa.id.apply(lambda id: int(id)),
    automount=True,
)

ginstoo_europa_a = digitalocean.DnsRecord(
    "ginstoo_europa_A",
    domain="ginstoo.net",
    name="europa",
    ttl=3600,
    type="A",
    value=europa_ip.ip_address,
    opts=pulumi.ResourceOptions(),
)

ginstoo_media_cname = digitalocean.DnsRecord(
    "ginstoo_media_CNAME",
    domain="ginstoo.net",
    name="media",
    ttl=3600,
    type="CNAME",
    value="europa.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)
