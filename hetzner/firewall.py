import pulumi
import pulumi_hcloud as hcloud

from utils import IP_ADDRESSES

from .europa import europa
from .hygiea import hygiea
from .kuiper import kuiper

# from .medina import medina
from .ook import ook

# from .laplace import laplace


# Allow outgoing DNS, pings, http(s), ntp
base_firewall = hcloud.Firewall(
    "base-firewall",
    apply_tos=[
        hcloud.FirewallApplyToArgs(
            server=30392406,  # medina (old)
        ),
        hcloud.FirewallApplyToArgs(
            server=europa.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=hygiea.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=kuiper.id,
        ),
        # hcloud.FirewallApplyToArgs(
        #     server=laplace.id,
        # ),
        hcloud.FirewallApplyToArgs(
            server=ook.id,
        ),
    ],
    name="base-firewall",
    rules=[
        hcloud.FirewallRuleArgs(
            description="allow outgoing DNS TCP requests",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="53",
            protocol="tcp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow outgoing pings",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            protocol="icmp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow outgoing DNS UDP requests",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="53",
            protocol="udp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow incoming pings",
            direction="in",
            protocol="icmp",
            source_ips=["0.0.0.0/0"],
        ),
        hcloud.FirewallRuleArgs(
            description="allow outgoing http requests",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="80",
            protocol="tcp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow outgoing https requests",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="443",
            protocol="tcp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow outgoing ntp requests over udp",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="123",
            protocol="udp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow outgoing ntp requests over tcp",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="123",
            protocol="tcp",
        ),
    ],
    opts=pulumi.ResourceOptions(),
)

# Allow incoming http(s)
webserver_firewall = hcloud.Firewall(
    "webserver-firewall",
    apply_tos=[
        hcloud.FirewallApplyToArgs(
            server=30392406,  # medina (old)
        ),
        hcloud.FirewallApplyToArgs(
            server=europa.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=hygiea.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=kuiper.id,
        ),
        # hcloud.FirewallApplyToArgs(
        #     server=laplace.id,
        # ),
    ],
    name="webserver-firewall",
    rules=[
        hcloud.FirewallRuleArgs(
            description="allow incoming http requests",
            direction="in",
            port="80",
            protocol="tcp",
            source_ips=["0.0.0.0/0"],
        ),
        hcloud.FirewallRuleArgs(
            description="allow incoming https requests",
            direction="in",
            port="443",
            protocol="tcp",
            source_ips=["0.0.0.0/0"],
        ),
    ],
    opts=pulumi.ResourceOptions(),
)

# Allow UDP traffic on wireguard port
wireguard_firewall = hcloud.Firewall(
    "wireguard-firewall",
    apply_tos=[
        hcloud.FirewallApplyToArgs(
            server=30392406,  # medina (old)
        ),
        # hcloud.FirewallApplyToArgs(
        #     server=medina.id,
        # ),
    ],
    name="wireguard-firewall",
    rules=[
        hcloud.FirewallRuleArgs(
            description="allow outgoing wireguard udp traffic",
            destination_ips=["0.0.0.0/0"],
            direction="out",
            port="51820",
            protocol="udp",
        ),
        hcloud.FirewallRuleArgs(
            description="allow incoming wireguard udp traffic",
            direction="in",
            port="51820",
            protocol="udp",
            source_ips=["0.0.0.0/0"],
        ),
    ],
    opts=pulumi.ResourceOptions(),
)

# Allow SSH traffic from ip allowlist
ssh_from_fishers_firewall = hcloud.Firewall(
    "ssh-from-fishers-firewall",
    apply_tos=[
        hcloud.FirewallApplyToArgs(
            server=30392406,  # medina (old)
        ),
        hcloud.FirewallApplyToArgs(
            server=europa.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=hygiea.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=kuiper.id,
        ),
        # hcloud.FirewallApplyToArgs(
        #     server=laplace.id,
        # ),
    ],
    name="ssh-from-fishers",
    rules=[
        hcloud.FirewallRuleArgs(
            description="allow incoming ssh traffic from fishers",
            direction="in",
            port="22",
            protocol="tcp",
            source_ips=IP_ADDRESSES,
        )
    ],
    opts=pulumi.ResourceOptions(),
)

# Allow outgoing SSH traffic
ssh_firewall = hcloud.Firewall(
    "ssh-firewall",
    apply_tos=[
        hcloud.FirewallApplyToArgs(
            server=europa.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=kuiper.id,
        ),
    ],
    name="ssh-out",
    rules=[
        hcloud.FirewallRuleArgs(
            description="allow ssh outgoing traffic",
            direction="out",
            port="22",
            protocol="tcp",
            destination_ips=["0.0.0.0/0"],
        )
    ],
)

# Allow outgoing telnet traffic
telnet_firewall = hcloud.Firewall(
    "telnet-firewall",
    apply_tos=[
        hcloud.FirewallApplyToArgs(
            server=europa.id,
        ),
        hcloud.FirewallApplyToArgs(
            server=kuiper.id,
        ),
    ],
    name="telnet-out",
    rules=[
        hcloud.FirewallRuleArgs(
            description="allow telnet outgoing traffic",
            direction="out",
            port="23",
            protocol="tcp",
            destination_ips=["0.0.0.0/0"],
        )
    ],
)

# Allow mosquitto traffic
# mosquitto_firewall = hcloud.Firewall(
#     "mosquitto-firewall",
#     apply_tos=[
#         hcloud.FirewallApplyToArgs(
#             server=laplace.id,
#         ),
#     ],
#     # name="mosquitto-firewall",
#     rules=[
#         hcloud.FirewallRuleArgs(
#             description="allow outgoing mosquitto tcp traffic",
#             destination_ips=["0.0.0.0/0"],
#             direction="out",
#             port="4883",
#             protocol="udp",
#         ),
#         hcloud.FirewallRuleArgs(
#             description="allow incoming mosquitto tcp traffic",
#             direction="in",
#             port="4883",
#             protocol="udp",
#             source_ips=["0.0.0.0/0"],
#         ),
#     ],
#     opts=pulumi.ResourceOptions(),
# )
