"""thoth  - internal gitlab server.

Thoth Station was used for research on the protomolecule by Protgen Corporation.
"""

import pulumi
import pulumi_hcloud as hcloud

from .networks import hetzner_network, hetzner_network_subnet
from .sshkeys import stooj_yubikey5_hcloud
from .firewall import base_firewall


LOCATION = "fsn1"
DATACENTER = f"{LOCATION}-dc14"
IMAGE = "ubuntu-22.04"
SERVER_TYPE = "cpx21"

thoth_ip = hcloud.PrimaryIp(
    "thoth-primary-ip",
    datacenter=DATACENTER,
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
)

thoth = hcloud.Server(
    "thoth",
    backups=True,
    datacenter=DATACENTER,
    firewall_ids=[base_firewall.id],
    image=IMAGE,
    name="thoth",
    server_type=SERVER_TYPE,
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=thoth_ip.id,
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id,
            ip="172.16.48.8",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id]
)
