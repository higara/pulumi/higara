"""medina - wireguard and dns for higara.

Medina
Currently decommissioned.
"""

import pulumi
import pulumi_digitalocean as digitalocean
import pulumi_hcloud as hcloud

from .networks import hetzner_network
from .sshkeys import stooj_yubikey5_hcloud

medina_ip = hcloud.PrimaryIp(
    "medina-primary_ip",
    datacenter="fsn1-dc14",
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
    opts=pulumi.ResourceOptions(),
)

medina = hcloud.Server(
    "medinatemp",
    backups=False,
    image="ubuntu-22.04",
    location="fsn1",
    name="medinatemp",
    server_type="cpx11",
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=medina_ip.id,
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id,
            ip="172.16.48.10",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id],
    opts=pulumi.ResourceOptions(),
)

ginstoo_medinatemp_a = digitalocean.DnsRecord(
    "ginstoo_medinatemp_A",
    domain="ginstoo.net",
    name="medinatemp",
    ttl=3600,
    type="A",
    value=medina_ip.ip_address,
    opts=pulumi.ResourceOptions(),
)

ginstoo_wgtemp_cname = digitalocean.DnsRecord(
    "ginstoo_wgtemp_a",
    domain="ginstoo.net",
    name="wgtemp",
    ttl=3600,
    type="CNAME",
    value="medinatemp.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)
