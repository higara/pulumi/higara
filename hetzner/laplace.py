"""
laplace - Owntracks server

Darkside Point (originally named Observatory Laplace-Epsilon) is a lagrange station orbiting at the Sun-Earth L2 Lagrange point - making it permanently shrouded in Earth's shadow, never receiving light or radiation from the sun. This position made it ideal for use as an observatory looking out into deep space, using infrared means to look at other stars - though it is one of only a few permanent habitats situated at L2.

"""

import pulumi
import pulumi_digitalocean as digitalocean
import pulumi_hcloud as hcloud

from .networks import hetzner_network
from .sshkeys import stooj_yubikey5_hcloud

LOCATION = "fsn1"
IMAGE = "ubuntu-22.04"
SERVER_TYPE = "cx22"

laplace_ip = hcloud.PrimaryIp(
    "laplace-primary_ip",
    datacenter="fsn1-dc14",
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
    opts=pulumi.ResourceOptions(),
)

laplace = hcloud.Server(
    "laplace",
    backups=True,
    image=IMAGE,
    location=LOCATION,
    name="laplace",
    server_type=SERVER_TYPE,
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=laplace_ip.id.apply(lambda id: int(id)),
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id.apply(lambda id: int(id)),
            ip="172.16.48.11",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id],
    opts=pulumi.ResourceOptions(),
)

ginstoo_laplace_a = digitalocean.DnsRecord(
    "ginstoo_laplace_A",
    domain="ginstoo.net",
    name="laplace",
    ttl=3600,
    type="A",
    value=laplace_ip.ip_address,
    opts=pulumi.ResourceOptions(),
)

ginstoo_where_cname = digitalocean.DnsRecord(
    "ginstoo_where_CNAME",
    domain="ginstoo.net",
    name="where",
    ttl=3600,
    type="CNAME",
    value="laplace.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)
