"""ook - paperless server for ginstoo."""

import pulumi
import pulumi_hcloud as hcloud

from .networks import hetzner_network
from .sshkeys import stooj_yubikey5_hcloud

ook_ip = hcloud.PrimaryIp(
    "primary_ip-29435303",
    datacenter="fsn1-dc14",
    type="ipv4",
    assignee_type="server",
    auto_delete=True,
    opts=pulumi.ResourceOptions(import_="29435303"),
)

ook = hcloud.Server(
    "ook",
    backups=True,
    image="ubuntu-22.04",
    location="fsn1",
    name="ook",
    server_type="cpx11",
    public_nets=[
        hcloud.ServerPublicNetArgs(
            ipv4=ook_ip.id,
            ipv4_enabled=True,
            ipv6_enabled=False,
        ),
    ],
    networks=[
        hcloud.ServerNetworkArgs(
            network_id=hetzner_network.id,
            ip="172.16.48.7",
        ),
    ],
    ssh_keys=[stooj_yubikey5_hcloud.id],
    opts=pulumi.ResourceOptions(
        import_="32085006",
        ignore_changes=["networks", "public_nets", "ssh_keys"],
    ),
)
