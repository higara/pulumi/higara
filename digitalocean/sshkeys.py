import pulumi
import pulumi_digitalocean as digitalocean

config = pulumi.Config()
stooj_yubikey5_value = config.require("stooj-yubikey5")

stooj_yubikey5_do = digitalocean.SshKey(
    "stooj-yubikey5",
    name="stooj@yubikey5",
    public_key=stooj_yubikey5_value,
    opts=pulumi.ResourceOptions()
)
