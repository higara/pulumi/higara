import pulumi
import pulumi_digitalocean as digitalocean

ginstoo = digitalocean.Domain(
    "ginstoo", name="ginstoo.net", opts=pulumi.ResourceOptions()
)

ginstoo_soa = digitalocean.DnsRecord(
    "@",
    domain="ginstoo.net",
    name="@",
    ttl=1800,
    type="SOA",
    value="1800",
    opts=pulumi.ResourceOptions()
)

ginstoo_ns1 = digitalocean.DnsRecord(
    "ginstoo_ns1",
    domain="ginstoo.net",
    name="@",
    ttl=1800,
    type="NS",
    value="ns1.digitalocean.com.",
    opts=pulumi.ResourceOptions()
)

ginstoo_ns2 = digitalocean.DnsRecord(
    "ginstoo_ns2",
    domain="ginstoo.net",
    name="@",
    ttl=1800,
    type="NS",
    value="ns2.digitalocean.com.",
    opts=pulumi.ResourceOptions()
)

ginstoo_ns3 = digitalocean.DnsRecord(
    "ginstoo_ns3",
    domain="ginstoo.net",
    name="@",
    ttl=1800,
    type="NS",
    value="ns3.digitalocean.com.",
    opts=pulumi.ResourceOptions()
)

ginstoo_protonmail_verification_txt = digitalocean.DnsRecord(
    "ginstoo_protonmail-verification_TXT",
    domain="ginstoo.net",
    name="@",
    ttl=3600,
    type="TXT",
    value="protonmail-verification=fd557468c46ed4a8aaf5eb42f29ddbc34d13648e",
    opts=pulumi.ResourceOptions()
)

ginstoo_google_search_verification_txt = digitalocean.DnsRecord(
    "ginstoo_google-search-verification_TXT",
    domain="ginstoo.net",
    name="@",
    ttl=3600,
    type="TXT",
    value="google-site-verification=RNhS7tC9rv94yQRePri-eD8KXF8-WqBLZGBflWeELdo",
    opts=pulumi.ResourceOptions()
)

ginstoo_mx1 = digitalocean.DnsRecord(
    "ginstoo_MX1",
    domain="ginstoo.net",
    name="@",
    priority=10,
    ttl=14400,
    type="MX",
    value="mail.protonmail.ch.",
    opts=pulumi.ResourceOptions()
)

ginstoo_mx2 = digitalocean.DnsRecord(
    "ginstoo_MX2",
    domain="ginstoo.net",
    name="@",
    priority=20,
    ttl=14400,
    type="MX",
    value="mailsec.protonmail.ch.",
    opts=pulumi.ResourceOptions()
)

ginstoo_protonmail_spf1 = digitalocean.DnsRecord(
    "ginstoo_protonmail-spf1",
    domain="ginstoo.net",
    name="@",
    ttl=3600,
    type="TXT",
    value="v=spf1 include:_spf.protonmail.ch mx ~all",
    opts=pulumi.ResourceOptions()
)

ginstoo_protonmail_domainkey_cname = digitalocean.DnsRecord(
    "ginstoo_protonmail-_domainkey_CNAME",
    domain="ginstoo.net",
    name="protonmail._domainkey",
    ttl=43200,
    type="CNAME",
    value="protonmail.domainkey.dgd5ovokvengvtjrrnoadvsl23icorgxwbpoj4e6p6m6ykcacoqeq.domains.proton.ch.",
    opts=pulumi.ResourceOptions()
)

ginstoo_protonmail2_domainkey_cname = digitalocean.DnsRecord(
    "ginstoo_protonmail2-_domainkey_CNAME",
    domain="ginstoo.net",
    name="protonmail2._domainkey",
    ttl=43200,
    type="CNAME",
    value="protonmail2.domainkey.dgd5ovokvengvtjrrnoadvsl23icorgxwbpoj4e6p6m6ykcacoqeq.domains.proton.ch.",
    opts=pulumi.ResourceOptions()
)

ginstoo_protonmail3_domainkey_cname = digitalocean.DnsRecord(
    "ginstoo_protonmail3-_domainkey_CNAME",
    domain="ginstoo.net",
    name="protonmail3._domainkey",
    ttl=43200,
    type="CNAME",
    value="protonmail3.domainkey.dgd5ovokvengvtjrrnoadvsl23icorgxwbpoj4e6p6m6ykcacoqeq.domains.proton.ch.",
    opts=pulumi.ResourceOptions()
)

ginstoo_dmarc_txt = digitalocean.DnsRecord(
    "ginstoo_dmarc_TXT",
    domain="ginstoo.net",
    name="_dmarc",
    ttl=3600,
    type="TXT",
    value="v=DMARC1; p=quanantine",
    opts=pulumi.ResourceOptions()
)

ginstoo_tendi_a = digitalocean.DnsRecord(
    "ginstoo_tendi_A",
    domain="ginstoo.net",
    name="tendi",
    ttl=3600,
    type="A",
    value="159.223.225.195",
    opts=pulumi.ResourceOptions()
)

ginstoo_code_cname = digitalocean.DnsRecord(
    "ginstoo_code_CNAME",
    domain="ginstoo.net",
    name="code",
    ttl=3200,
    type="CNAME",
    value="tendi.ginstoo.net.",
    opts=pulumi.ResourceOptions()
)

ginstoo_em813774_cname = digitalocean.DnsRecord(
    "ginstoo_em813774_CNAME",
    domain="ginstoo.net",
    name="em813774",
    ttl=3200,
    type="CNAME",
    value="return.smtp2go.net.",
    opts=pulumi.ResourceOptions()
)

ginstoo_s813774_domainkey_cname = digitalocean.DnsRecord(
    "ginstoo_s813774-_domainkey_CNAME",
    domain="ginstoo.net",
    name="s813774._domainkey",
    ttl=3200,
    type="CNAME",
    value="dkim.smtp2go.net.",
    opts=pulumi.ResourceOptions()
)

ginstoo_link_cname = digitalocean.DnsRecord(
    "ginstoo_link_CNAME",
    domain="ginstoo.net",
    name="link",
    ttl=3200,
    type="CNAME",
    value="track.smtp2go.net.",
    opts=pulumi.ResourceOptions()
)

ginstoo_nc_cname = digitalocean.DnsRecord(
    "ginstoo_nc_CNAME",
    domain="ginstoo.net",
    name="nc",
    ttl=3800,
    type="CNAME",
    value="nx36040.your-storageshare.de.",
    opts=pulumi.ResourceOptions()
)
