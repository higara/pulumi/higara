import pulumi
import pulumi_digitalocean as digitalocean

stooj_project = digitalocean.Project(
    "stooj",
    description="Update your project information under Settings",
    is_default=True,
    name="Stoo Johnston",
    purpose="",
    resources=[
        "do:domain:stooj.org",
        "do:domain:ginstoo.net",
        "do:droplet:327871196",
    ],
    opts=pulumi.ResourceOptions(),
)
