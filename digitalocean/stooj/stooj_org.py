import pulumi
import pulumi_digitalocean as digitalocean

stooj = digitalocean.Domain("stooj", name="stooj.org", opts=pulumi.ResourceOptions())

stooj_at_soa = digitalocean.DnsRecord(
    "stooj_at_SOA",
    domain="stooj.org",
    name="@",
    ttl=1800,
    type="SOA",
    value="1800",
    opts=pulumi.ResourceOptions(),
)

stooj_at_ns1 = digitalocean.DnsRecord(
    "stooj_at_NS",
    domain="stooj.org",
    name="@",
    ttl=1800,
    type="NS",
    value="ns1.digitalocean.com.",
    opts=pulumi.ResourceOptions(),
)

stooj_at_ns2 = digitalocean.DnsRecord(
    "stooj_at_NS_1",
    domain="stooj.org",
    name="@",
    ttl=1800,
    type="NS",
    value="ns2.digitalocean.com.",
    opts=pulumi.ResourceOptions(),
)

stooj_at_ns3 = digitalocean.DnsRecord(
    "stooj_at_NS3",
    domain="stooj.org",
    name="@",
    ttl=1800,
    type="NS",
    value="ns3.digitalocean.com.",
    opts=pulumi.ResourceOptions(),
)

stooj_at_a = digitalocean.DnsRecord(
    "stooj_at_A",
    domain="stooj.org",
    name="@",
    ttl=1800,
    type="A",
    value="46.101.35.51",
    opts=pulumi.ResourceOptions(),
)

stooj_tsoni_a = digitalocean.DnsRecord(
    "stooj_tsoni_A",
    domain="stooj.org",
    name="tsoni",
    ttl=1800,
    type="A",
    value="178.62.128.124",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_kira_a = digitalocean.DnsRecord(
    "stooj_kira_A",
    domain="stooj.org",
    name="kira",
    ttl=1800,
    type="A",
    value="82.196.14.145",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_hyperion_a = digitalocean.DnsRecord(
    "stooj_hyperion_A",
    domain="stooj.org",
    name="hyperion",
    ttl=1800,
    type="A",
    value="128.199.83.194",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_cleef_a = digitalocean.DnsRecord(
    "stooj_cleef_A",
    domain="stooj.org",
    name="cleef",
    ttl=3600,
    type="A",
    value="188.226.130.139",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_pris_a = digitalocean.DnsRecord(
    "stooj_pris_A",
    domain="stooj.org",
    name="pris",
    ttl=600,
    type="A",
    value="81.187.162.136",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_a_mx_a = digitalocean.DnsRecord(
    "stooj_a_mx_A",
    domain="stooj.org",
    name="a.mx",
    ttl=3600,
    type="A",
    value="188.226.130.139",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_eastwood_a = digitalocean.DnsRecord(
    "stooj_eastwood_A",
    domain="stooj.org",
    name="eastwood",
    ttl=3600,
    type="A",
    value="165.227.235.148",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_lspace_a = digitalocean.DnsRecord(
    "stooj_lspace_A",
    domain="stooj.org",
    name="lspace",
    ttl=3600,
    type="A",
    value="46.101.34.20",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_mail_cname = digitalocean.DnsRecord(
    "stooj_mail_CNAME",
    domain="stooj.org",
    name="mail",
    ttl=1800,
    type="CNAME",
    value="cleef.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_salt_cname = digitalocean.DnsRecord(
    "stooj_salt_CNAME",
    domain="stooj.org",
    name="salt",
    ttl=1800,
    type="CNAME",
    value="eastwood.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_dax_cname = digitalocean.DnsRecord(
    "stooj_dax_CNAME",
    domain="stooj.org",
    name="dax",
    ttl=1800,
    type="CNAME",
    value="odo.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_tunes_cname = digitalocean.DnsRecord(
    "stooj_tunes_CNAME",
    domain="stooj.org",
    name="tunes",
    ttl=1800,
    type="CNAME",
    value="jenellen.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_media_cname = digitalocean.DnsRecord(
    "stooj_media_CNAME",
    domain="stooj.org",
    name="media",
    ttl=43200,
    type="CNAME",
    value="pris.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_blog_cname = digitalocean.DnsRecord(
    "stooj_blog_CNAME",
    domain="stooj.org",
    name="blog",
    ttl=43200,
    type="CNAME",
    value="avasarala.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_docs_cname = digitalocean.DnsRecord(
    "stooj_docs_CNAME",
    domain="stooj.org",
    name="docs",
    ttl=43200,
    type="CNAME",
    value="lspace.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_autodiscover_cname = digitalocean.DnsRecord(
    "stooj_autodiscover_CNAME",
    domain="stooj.org",
    name="autodiscover",
    ttl=43200,
    type="CNAME",
    value="cleef.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_autoconfig_cname = digitalocean.DnsRecord(
    "stooj_autoconfig_CNAME",
    domain="stooj.org",
    name="autoconfig",
    ttl=43200,
    type="CNAME",
    value="cleef.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_irc_cname = digitalocean.DnsRecord(
    "stooj_irc_CNAME",
    domain="stooj.org",
    name="irc",
    ttl=43200,
    type="CNAME",
    value="pris.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_books_cname = digitalocean.DnsRecord(
    "stooj_books_CNAME",
    domain="stooj.org",
    name="books",
    ttl=43200,
    type="CNAME",
    value="pris.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

stooj_protonmail_domainkey_cname = digitalocean.DnsRecord(
    "stooj_protonmail_domainkey_CNAME",
    domain="stooj.org",
    name="protonmail._domainkey",
    ttl=43200,
    type="CNAME",
    value="protonmail.domainkey.dycor3o22z5ry5b3ayr6zspds7lazf32z2kqg6welwnxqbyxb7xya.domains.proton.ch.",
    opts=pulumi.ResourceOptions(),
)

stooj_protonmail2_domainkey_cname = digitalocean.DnsRecord(
    "stooj_protonmail2_domainkey_CNAME",
    domain="stooj.org",
    name="protonmail2._domainkey",
    ttl=43200,
    type="CNAME",
    value="protonmail2.domainkey.dycor3o22z5ry5b3ayr6zspds7lazf32z2kqg6welwnxqbyxb7xya.domains.proton.ch.",
    opts=pulumi.ResourceOptions(),
)

stooj_protonmail3_domainkey_cname = digitalocean.DnsRecord(
    "stooj_protonmail3_domainkey_CNAME",
    domain="stooj.org",
    name="protonmail3._domainkey",
    ttl=43200,
    type="CNAME",
    value="protonmail3.domainkey.dycor3o22z5ry5b3ayr6zspds7lazf32z2kqg6welwnxqbyxb7xya.domains.proton.ch.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_odo_cname = digitalocean.DnsRecord(
    "stooj_odo_CNAME",
    domain="stooj.org",
    name="odo",
    ttl=3400,
    type="CNAME",
    value="code.ginstoo.net.",
    opts=pulumi.ResourceOptions(),
)

stooj_at_mx1 = digitalocean.DnsRecord(
    "stooj_at_MX_1",
    domain="stooj.org",
    name="@",
    priority=10,
    ttl=14400,
    type="MX",
    value="mail.protonmail.ch.",
    opts=pulumi.ResourceOptions(),
)

stooj_at_mx2 = digitalocean.DnsRecord(
    "stooj_at_MX_2",
    domain="stooj.org",
    name="@",
    priority=20,
    ttl=14400,
    type="MX",
    value="mailsec.protonmail.ch.",
    opts=pulumi.ResourceOptions(),
)

# TODO: Delete
stooj_at_mx = digitalocean.DnsRecord(
    "stooj_at_MX",
    domain="stooj.org",
    name="@",
    priority=60,
    ttl=1800,
    type="MX",
    value="a.mx.stooj.org.",
    opts=pulumi.ResourceOptions(),
)

stooj_at_spf_txt = digitalocean.DnsRecord(
    "stooj_at_spf_TXT",
    domain="stooj.org",
    name="@",
    ttl=3600,
    type="TXT",
    value="v=spf1 include:_spf.protonmail.ch mx ~all",
    opts=pulumi.ResourceOptions(),
)

stooj_dmarc_txt = digitalocean.DnsRecord(
    "stooj_dmarc_TXT",
    domain="stooj.org",
    name="_dmarc",
    ttl=3600,
    type="TXT",
    value="v=DMARC1; p=none; rua=mailto:dmarc@stooj.org; ruf=mailto:dmarc@stooj.org; sp=reject; ri=86400",
    opts=pulumi.ResourceOptions(),
)

stooj_default_domainkey_txt = digitalocean.DnsRecord(
    "stooj_default_domainkey_TXT",
    domain="stooj.org",
    name="default._domainkey",
    ttl=3600,
    type="TXT",
    value="v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7egIa5+R4Vy3Hjso9c6Tpw3sT06V2VVEzYbT6jjMwQS9mQVuEBw8jBxkW3ptL8/66+xp4jOolf28S72NKQejsuXEeMBaFODP2M+Mih9R+1WWa2F28kjCjr794PryHq7Q1xwWENsjfjDkmVjmMTSw7tghG2NnMjkx1iafMnxXgOO4r+X07tObkajgiLd+S/TLr4UumprtxVTKw1A1wYZ7mSNaNJVXX9OOaum9FI+O+6n4KCZBDNHTvlz/2l4Nj1dOd0xW1c9zG1TjtZFEu4/sIPEPV6ZlRQwQd+N5+MPhTtSUq0CJKWIjeyduSg7kerx2eWL57EzOcufkeUh2h8QewQIDAQAB",
    opts=pulumi.ResourceOptions(),
)

stooj_keybase_txt = digitalocean.DnsRecord(
    "stooj_keybase_TXT",
    domain="stooj.org",
    name="_keybase",
    ttl=3600,
    type="TXT",
    value="keybase-site-verification=hYXLNVqNcOvYOMaOBMnN6vIn4Pp14NOmZ_RN7KvxpKk",
    opts=pulumi.ResourceOptions(),
)

stooj_at_google_verification_txt = digitalocean.DnsRecord(
    "stooj_at_google_verification_TXT",
    domain="stooj.org",
    name="@",
    ttl=3600,
    type="TXT",
    value="google-site-verification=9QhaAgRPiEe9it_7ss554Kv7Z-DMMA5pckcIJBrr2Yw",
    opts=pulumi.ResourceOptions(),
)

stooj_at_protonmail_verification_txt = digitalocean.DnsRecord(
    "stooj_at_protonmail_verification_TXT",
    domain="stooj.org",
    name="@",
    ttl=3600,
    type="TXT",
    value="protonmail-verification=ca181bd438d11c040717bd95edf38d7b89cabc7e",
    opts=pulumi.ResourceOptions(),
)
