import pulumi
import pulumi_digitalocean as digitalocean

tendi = digitalocean.Droplet(
    "tendi",
    backups=True,
    image="118857366",
    monitoring=True,
    name="tendi",
    region="ams3",
    size="s-2vcpu-4gb",
    tags=[
        "gitlab",
        "ssh",
        "firewalled",
    ],
    vpc_uuid="2be7b6e7-a0e3-4c6b-919b-af2252b7e44f",
    opts=pulumi.ResourceOptions(),
)
