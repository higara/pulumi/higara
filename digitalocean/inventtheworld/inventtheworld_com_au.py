import pulumi
import pulumi_digitalocean as digitalocean

inventtheworld = digitalocean.Domain(
    "inventtheworld",
    name="inventtheworld.com.au",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_ns1 = digitalocean.DnsRecord(
    "inventtheworld_ns1",
    domain="inventtheworld.com.au",
    name="@",
    ttl=1800,
    type="NS",
    value="ns1.digitalocean.com.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_ns2 = digitalocean.DnsRecord(
    "inventtheworld_ns2",
    domain="inventtheworld.com.au",
    name="@",
    ttl=1800,
    type="NS",
    value="ns2.digitalocean.com.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_ns3 = digitalocean.DnsRecord(
    "inventtheworld_ns3",
    domain="inventtheworld.com.au",
    name="@",
    ttl=1800,
    type="NS",
    value="ns3.digitalocean.com.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_a = digitalocean.DnsRecord(
    "inventtheworld_A",
    domain="inventtheworld.com.au",
    name="@",
    ttl=1800,
    type="A",
    value="146.185.140.65",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_mx1 = digitalocean.DnsRecord(
    "inventtheworld_MX1",
    domain="inventtheworld.com.au",
    name="@",
    priority=10,
    ttl=1800,
    type="MX",
    value="a.mx.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_spf_txt = digitalocean.DnsRecord(
    "inventtheworld_spf_TXT",
    domain="inventtheworld.com.au",
    name="@",
    ttl=1800,
    type="TXT",
    value="v=spf1 mx a:hyperion.inventtheworld.com.au include:servers.mcsv.net ~all",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_sara_a = digitalocean.DnsRecord(
    "inventtheworld_sara_A",
    domain="inventtheworld.com.au",
    name="sara",
    ttl=1800,
    type="A",
    value="182.54.232.229",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_wild_sara_a = digitalocean.DnsRecord(
    "inventtheworld_wild_-sara_A",
    domain="inventtheworld.com.au",
    name="*.sara",
    ttl=1800,
    type="A",
    value="182.54.232.229",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_mumble_cname = digitalocean.DnsRecord(
    "inventtheworld_mumble_CNAME",
    domain="inventtheworld.com.au",
    name="mumble",
    ttl=1800,
    type="CNAME",
    value="jenellen.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_www_cname = digitalocean.DnsRecord(
    "inventtheworld_www_CNAME",
    domain="inventtheworld.com.au",
    name="www",
    ttl=1800,
    type="CNAME",
    value="@",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_velator_a = digitalocean.DnsRecord(
    "inventtheworld_velator_A",
    domain="inventtheworld.com.au",
    name="velator",
    ttl=1800,
    type="A",
    value="95.85.41.219",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_mm_cname = digitalocean.DnsRecord(
    "inventtheworld_mm_CNAME",
    domain="inventtheworld.com.au",
    name="mm",
    ttl=1800,
    type="CNAME",
    value="velator.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_reaper_a = digitalocean.DnsRecord(
    "inventtheworld_reaper_A",
    domain="inventtheworld.com.au",
    name="reaper",
    ttl=1800,
    type="A",
    value="128.199.103.145",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_files_cname = digitalocean.DnsRecord(
    "inventtheworld_files_CNAME",
    domain="inventtheworld.com.au",
    name="files",
    ttl=1800,
    type="CNAME",
    value="ibis.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_audrid_a = digitalocean.DnsRecord(
    "inventtheworld_audrid_A",
    domain="inventtheworld.com.au",
    name="audrid",
    ttl=1800,
    type="A",
    value="146.185.140.65",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_joran_a = digitalocean.DnsRecord(
    "inventtheworld_joran_A",
    domain="inventtheworld.com.au",
    name="joran",
    ttl=1800,
    type="A",
    value="146.185.152.75",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_analytics_cname = digitalocean.DnsRecord(
    "inventtheworld_analytics_CNAME",
    domain="inventtheworld.com.au",
    name="analytics",
    ttl=1800,
    type="CNAME",
    value="joran.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_lela_a = digitalocean.DnsRecord(
    "inventtheworld_lela_A",
    domain="inventtheworld.com.au",
    name="lela",
    ttl=1800,
    type="A",
    value="128.199.243.175",
    opts=pulumi.ResourceOptions(),
)

inventtheworld_salt_cname = digitalocean.DnsRecord(
    "inventtheworld_salt_CNAME",
    domain="inventtheworld.com.au",
    name="salt",
    ttl=1800,
    type="CNAME",
    value="lela.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_k1_domainkey_cname = digitalocean.DnsRecord(
    "inventtheworld_k1-_domainkey_CNAME",
    domain="inventtheworld.com.au",
    name="k1._domainkey",
    ttl=1800,
    type="CNAME",
    value="dkim.mcsv.net.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_jenellen_a = digitalocean.DnsRecord(
    "inventtheworld_jenellen_A",
    domain="inventtheworld.com.au",
    name="jenellen",
    ttl=1800,
    type="A",
    value="139.59.252.101",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_fb_digital_classroom_cname = digitalocean.DnsRecord(
    "inventtheworld_fb-digital-classroom_CNAME",
    domain="inventtheworld.com.au",
    name="fb-digital-classroom",
    ttl=1800,
    type="CNAME",
    value="nyreen.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_staff_homebase_cname = digitalocean.DnsRecord(
    "inventtheworld_staff-homebase_CNAME",
    domain="inventtheworld.com.au",
    name="staff-homebase",
    ttl=1800,
    type="CNAME",
    value="zaeed.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_deworde_a = digitalocean.DnsRecord(
    "inventtheworld_deworde_A",
    domain="inventtheworld.com.au",
    name="deworde",
    ttl=1800,
    type="A",
    value="128.199.196.71",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_kids_cname = digitalocean.DnsRecord(
    "inventtheworld_kids_CNAME",
    domain="inventtheworld.com.au",
    name="kids",
    ttl=1800,
    type="CNAME",
    value="deworde.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_hyperion_a = digitalocean.DnsRecord(
    "inventtheworld_hyperion_A",
    domain="inventtheworld.com.au",
    name="hyperion",
    ttl=3600,
    type="A",
    value="128.199.66.190",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_mail_cname = digitalocean.DnsRecord(
    "inventtheworld_mail_CNAME",
    domain="inventtheworld.com.au",
    name="mail",
    ttl=43200,
    type="CNAME",
    value="hyperion.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_mx2 = digitalocean.DnsRecord(
    "inventtheworld_MX2",
    domain="inventtheworld.com.au",
    name="@",
    priority=100,
    ttl=14400,
    type="MX",
    value="b.mx.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_dmarc_txt = digitalocean.DnsRecord(
    "inventtheworld_dmarc_TXT",
    domain="inventtheworld.com.au",
    name="_dmarc",
    ttl=3600,
    type="TXT",
    value="v=DMARC1; p=none; rua=mailto:dmarc@inventtheworld.com.au; ruf=mailto:dmarc@inventtheworld.com.au; sp=reject; ri=86400",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_mail_domainkey_txt = digitalocean.DnsRecord(
    "inventtheworld_mail-_domainkey_TXT",
    domain="inventtheworld.com.au",
    name="mail._domainkey",
    ttl=3600,
    type="TXT",
    value="v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZsSM27X1CG7q1F/c98cqmHgFBeoIxqCQOn5TllroUWtWCKnPvYsfcG0sETUKDcczSo1DPlcAzp+PaTSMG4MDHHVQl71jyxfzFNeI8XMUMBZqmhv5XbS6g0TJ6KvIbMsarM3GwyAem0KaDAu93edE4o7102Wrng/M9RQHm2fmJpQIDAQAB",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_b_mx_a = digitalocean.DnsRecord(
    "inventtheworld_b-mx_A",
    domain="inventtheworld.com.au",
    name="b.mx",
    ttl=3600,
    type="A",
    value="128.199.66.190",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_a_mx_a = digitalocean.DnsRecord(
    "inventtheworld_a-mx_A",
    domain="inventtheworld.com.au",
    name="a.mx",
    ttl=3600,
    type="A",
    value="128.199.66.190",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_ibis_a = digitalocean.DnsRecord(
    "inventtheworld_ibis_A",
    domain="inventtheworld.com.au",
    name="ibis",
    ttl=3600,
    type="A",
    value="128.199.241.207",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_autodiscover_cname = digitalocean.DnsRecord(
    "inventtheworld_autodiscover_CNAME",
    domain="inventtheworld.com.au",
    name="autodiscover",
    ttl=43200,
    type="CNAME",
    value="hyperion.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_autoconfig_cname = digitalocean.DnsRecord(
    "inventtheworld_autoconfig_CNAME",
    domain="inventtheworld.com.au",
    name="autoconfig",
    ttl=43200,
    type="CNAME",
    value="hyperion.inventtheworld.com.au.",
    opts=pulumi.ResourceOptions(),
)


inventtheworld_google_site_verification_txt = digitalocean.DnsRecord(
    "inventtheworld_google-site-verification_TXT",
    domain="inventtheworld.com.au",
    name="@",
    ttl=3600,
    type="TXT",
    value="google-site-verification=wp7ddKFMI0AgW_S27nY1nsCZfL5SUV_IfxJIUKhe9Qk",
    opts=pulumi.ResourceOptions(),
)
