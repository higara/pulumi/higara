import pulumi
import pulumi_digitalocean as digitalocean

hyperion = digitalocean.Droplet(
    "hyperion",
    backups=True,
    image="28415447",
    monitoring=True,
    name="hyperion.inventtheworld.com.au",
    region="sgp1",
    size="s-2vcpu-4gb",
    tags=[
        "modoboa",
        "mailserver",
        "ssh",
        "webserver",
        "firewalled",
    ],
    vpc_uuid="280c0df0-dc83-11e8-beb4-3cfdfea9fb81",
    opts=pulumi.ResourceOptions(),
)
