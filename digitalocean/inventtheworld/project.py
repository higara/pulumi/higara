import pulumi
import pulumi_digitalocean as digitalocean

inventtheworld = digitalocean.Project(
    "inventtheworld",
    name="Invent the World",
    purpose="Other",
    resources=[
        "do:domain:inventtheworld.com.au",
        "do:droplet:68886857",
    ],
    opts=pulumi.ResourceOptions(),
)
