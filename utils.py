import csv

import requests


def get_starlink_ips(current_locations):
    starlink_feed = "https://geoip.starlinkisp.net/feed.csv"
    starlink_all_ips_csv = requests.get(starlink_feed)
    fieldnames = ["iprange", "country_code", "location_code", "city"]
    csvreader = csv.DictReader(starlink_all_ips_csv.text.split(), fieldnames=fieldnames)
    return [
        row["iprange"] for row in csvreader if row["country_code"] in current_locations
    ]


CURRENT_LOCATIONS = ["ES"]
HOME_IP = [
    "91.242.154.177/32",
    "178.237.239.49/32",
    "90.114.73.20/32",
    "178.237.234.49/32",
    "88.17.125.160/32",
    "83.49.216.147/32",
]
COWORKING_IP_ADDRESSES = [
    "88.17.126.18/32",
    "83.49.214.173/32",
    "88.17.120.122/32",
    "88.19.69.49/32",
]
STARLINK_IPS = get_starlink_ips(CURRENT_LOCATIONS)

IP_ADDRESSES = HOME_IP + COWORKING_IP_ADDRESSES + STARLINK_IPS
